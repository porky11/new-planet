use shaders;

use Mesh;

use glium::uniforms::Uniforms;
use glium;
use glium::{Display,Program,Frame};



pub fn draw_mesh<U: Uniforms>(display: &Display, target: &mut Frame, mesh: &Mesh, uniforms: &U) {
    static mut PROGRAM: Option<Rc<Program>> = None;
    
    use std::rc::Rc;

    fn get_program(display: &Display) -> Rc<Program> {
        unsafe {
            if PROGRAM.is_none() {
                PROGRAM = Some(
                    Rc::new(
                        glium::Program::from_source(display,
                        shaders::mesh::VS,
                        shaders::mesh::FS,
                        None).unwrap()
                    )
                )
            }
            if let Some(ref program) = PROGRAM {
                program.clone()
            } else {
                panic!()
            }
        }
    }
    let program = get_program(display);
    
    let params = glium::DrawParameters {
        depth: glium::Depth {
            test: glium::draw_parameters::DepthTest::IfLess,
            write: true,
            .. Default::default()
        },
        .. Default::default()
    };

    use glium::Surface;
    target.draw(&glium::vertex::VertexBuffer::new(display, &mesh.vertices).unwrap(),
                    &glium::index::IndexBuffer::new(display, glium::index::PrimitiveType::TrianglesList, &mesh.indices[..]).unwrap(),
                    &*program, uniforms, &params).unwrap();
}

