extern crate ga;
extern crate num_traits;
extern crate physics;
extern crate obj;
extern crate cgmath;
#[macro_use]
extern crate glium;
extern crate vector_space;



mod shaders;
mod draw;
mod world;

use glium::{Display,Frame};
use std::rc::Rc;

use num_traits::{Zero,One,Num};
use num_traits::real::Real;

use std::ops::Neg;

use world::World;

use ga::{
    Vector3 as Vector,
    Bivector3 as Bivector,
    Rotor3 as Rotor,
    Pseudoscalar3 as Pseudoscalar,
};

use physics::transform::{
    Transform,
    Accelerate
};

trait Update {
    fn update(&mut self);
}


struct Spatial<T: Copy+Num+Neg<Output=T>,S> {
    pos: Vector<T>,
    vel: Vector<T>,
    acc: Vector<T>,

    dir: Rotor<T>,
    rot: Bivector<T>,
    boo: Bivector<T>,
    
    frag: T,
    shape: S,
    //mesh: Rc<Mesh>
}

use physics::collide::Collide;
use physics::shapes::{CollisionInfo,CollisionData};

impl<T: PartialOrd+Real+AddAssign,S: CollisionInfo<Vector<T>,S>> Collide for Spatial<T,S> {
    fn collide(&mut self, other: &mut Self) {
        let info = self.shape.collision_info(&other.shape, self.pos, other.pos);
        if let Some(data) = info {
            use ga::{Inner,Outer};

            let mass = T::one()/(self.frag+other.frag);
            let two = T::one()+T::one();

            let acc = data.vector;
            
            let dvel = other.vel-self.vel;
            
            let rim_rot_self = self.rot*data.dis_self;
            let rim_rot_other = other.rot*data.dis_other;
            
            let dir = data.vector.normalize();
            
            let rim_vel_self = rim_rot_self.inner(dir);
            let rim_vel_other = rim_rot_other.inner(dir);
            
            let rim_svel = rim_vel_self+rim_vel_other+dvel.outer(dir).inner(dir);


            //let rim_srot = dvel.outer(dir)+rim_rot_self.inner(dir).outer(dir)+rim_rot_other.inner(dir).outer(dir);
            //let rim_drot = -rim_rot_self+rim_rot_other;
            
            let rim_srot = rim_svel.outer(dir);
            
            //let rim_svel = dvel.outer(dir).inner(dir)+rim_vel_a+rim_vel_b;

            let acc = acc+rim_srot.inner(dir)/two;
            
            self.accelerate2(other, acc);
            
            let rot_acc = -rim_srot*mass/two;

            let rot_acc_self = rot_acc/data.dis_self;
            let rot_acc_other = rot_acc/data.dis_other;
            
            self.accelerate(rot_acc_self);
            other.accelerate(rot_acc_other);
            
            let other_mag = other.rot.magnitude();
            if !other_mag.is_zero() {
                let other_plane = other.rot.normalize();
                let self_rot = self.rot.inner(other_plane).outer(-other_plane);
                let drot = other.rot-self_rot;
                let drot = drot*drot.outer(dir).magnitude();
                self.accelerate2(other,drot);
            }

            let self_mag = self.rot.magnitude();
            if !self_mag.is_zero() {
                let self_plane = self.rot.normalize();
                let other_rot = other.rot.inner(self_plane).outer(-self_plane);
                let drot = self.rot-other_rot;
                let drot = drot*drot.outer(dir).magnitude();
                other.accelerate2(self,drot);
            }
            
        }
    }
}

impl<T: Copy+Num+Neg<Output=T>+AddAssign<T>,S> Accelerate<Vector<T>> for Spatial<T,S> {
    fn accelerate(&mut self, acc: Vector<T>) {
        self.acc.transform(acc*self.frag)
    }
    fn accelerate2(&mut self, other: &mut Self, acc: Vector<T>) {
        let mass = T::one()/(self.frag+other.frag);
        self.accelerate(acc*mass);
        other.accelerate(-acc*mass);
    }
}

impl<T: Copy+Num+Neg<Output=T>+AddAssign<T>,S> Accelerate<Bivector<T>> for Spatial<T,S> {
    fn accelerate(&mut self, acc: Bivector<T>) {
        self.boo.transform(acc*self.frag)
    }
    fn accelerate2(&mut self, other: &mut Self, acc: Bivector<T>) {
        let mass = T::one()/(self.frag+other.frag);
        self.accelerate(acc*mass);
        other.accelerate(-acc*mass);
    }
}

use std::ops::{AddAssign,MulAssign};

impl<T: Copy+Num+Neg<Output=T>+AddAssign<T>+MulAssign<T>+From<f32>,S> Update for Spatial<T,S> {
    fn update(&mut self) {
        self.vel.transform(self.acc);
        self.pos.transform(self.acc);
        self.pos.transform(self.vel);
        self.acc = Zero::zero();
        self.vel *= 0.99f32.into();

        self.rot.transform(self.boo);
        self.dir.transform(self.boo);
        self.dir.transform(self.rot);
        self.boo = Zero::zero();
        //self.rot *= 0.99f32.into();
    }
}

pub struct DrawContext<'a, 'b> {
    pub target: &'a mut Frame,
    pub display: &'b Display,
    pub view: [[f32;4];4],
    pub perspective: [[f32;4];4],
}

use physics::accessors::{
    Position
};

impl<T: Copy+Num+Neg<Output=T>,S> Position for Spatial<T,S> {
    type Type = Vector<T>;
    fn pos(&self) -> &Self::Type {
        &self.pos
    }
}

impl<T: Copy+Num+Neg<Output=T>,S: Default> Default for Spatial<T,S> {
    fn default() -> Self {
        Self {
            pos: Vector::zero(),
            vel: Vector::zero(),
            acc: Vector::zero(),

            dir: Rotor::one(),
            rot: Bivector::zero(),
            boo: Bivector::zero(),
            
            frag: T::one(),
            shape: S::default(),
        }
    }
}

pub struct Mesh {
    vertices: Vec<Vertex>,
    indices: Vec<u16>,
}

impl Mesh {
    fn new() -> Mesh {
        Mesh::new()
    }
    fn from_obj(path: &str) -> Mesh {
        use obj::{load_obj,Obj};
        use std::fs::File;
        use std::io::BufReader;
        let input = BufReader::new(File::open(path).unwrap());
        let obj: Obj = load_obj(input).unwrap();
        
        use std::mem::transmute;
        
        let vertices = unsafe {transmute::<_,Vec<Vertex>>(obj.vertices)};
        let indices = obj.indices;
        
        Mesh {
            vertices,
            indices
        }
    }
}

pub trait Draw {
    fn draw(&self, &mut DrawContext);
}

use physics::shapes::Ball;
use vector_space::InnerSpace;

impl Draw for Spatial<f32,Ball<f32>> {
    fn draw(&self, context: &mut DrawContext) {
        use std::mem::transmute;

        let model = 
            cgmath::Matrix4::from_translation(
                unsafe {transmute::<_, cgmath::Vector3<f32>>(self.pos)}
            );
        let model_x = self.dir.rotate(Vector::<f32>::e1()).normalize().vector();
        let model_y = self.dir.rotate(Vector::<f32>::e2()).normalize().vector();
        let model_z = self.dir.rotate(Vector::<f32>::e3()).normalize().vector();
        use cgmath::conv::array4x4;
        let uniforms = uniform!{
            model: array4x4(model),
            scale: self.shape.rad,
            model_rot: [model_x, model_y, model_z],
            perspective: context.perspective,
            view: context.view,
        };
        
        
        static mut MESH: Option<Rc<Mesh>> = None;

        fn get_mesh() -> Rc<Mesh> {
            unsafe {
                if MESH.is_none() {
                    MESH = Some(
                        Rc::new(Mesh::from_obj("sphere.obj"))
                    )
                }
                if let Some(ref mesh) = MESH {
                    mesh.clone()
                } else {
                    panic!()
                }
            }
        }
        
        let mesh = get_mesh();
        
        draw::draw_mesh(context.display, context.target, &*mesh, &uniforms);
    }
}




#[derive(Copy, Clone)]
#[repr(C)]
struct Vertex {
    pos: [f32;3],
    norm: [f32;3]
}

implement_vertex!(Vertex, pos, norm);


#[repr(C)]
struct Input {
    hor: i32,
    ver: i32,
    
    jump: i32,
    rest: i32,
}

fn game_loop(mut world: World<Spatial<f32,Ball<f32>>>) {

    let mut view_rot = Rotor::one();
    let mut view_pos = -Vector::e1()*16.0;
    

    let mut left = false;
    let mut right = false;
    let mut up = false;
    let mut down = false;

    let mut rleft = false;
    let mut rright = false;
    let mut rup = false;
    let mut rdown = false;

    let mut events_loop = glium::glutin::EventsLoop::new();
    
    let window = glium::glutin::WindowBuilder::new()
        .with_dimensions(1920, 1280);
    
    let context = glium::glutin::ContextBuilder::new();
    
    let display = glium::Display::new(window, context, &events_loop).unwrap();
    
    let mut closed = false;
    
    let mut perspective = cgmath::perspective(cgmath::Rad(std::f32::consts::PI/2.0), 1.0, 1.0, 65536.0);

    
    while !closed {
        use std::time;

        let now = time::Instant::now();
        
        let d = world.objects[0].pos-view_pos;
        
        let dn = d-d.normalize()*16.0;
        
        view_pos += dn*0.1;
        
        let vec = view_rot.rotate(d).normalize();
        let front = Vector::<f32>::e3();
        let rotor = (front.outer(-vec)*0.1).exp();
        //let rotor = front*vec;
        view_rot = (rotor*view_rot).normalize();
        
        let x = view_rot.reverse().rotate(Vector::<f32>::e1()).normalize();
        let y = view_rot.reverse().rotate(Vector::<f32>::e2()).normalize();
        let z = view_rot.reverse().rotate(Vector::<f32>::e3()).normalize();

        fn num(x: bool) -> f32 {
            if x {1.0} else {0.0}
        }

        let vec = 
            x*(num(right)-num(left))*0.01+
            y*(num(up)-num(down))*0.01-
            z*(num(rup)-num(rdown))*0.01;
        
        world.objects[0].accelerate(vec);
        
        use ga::Outer;
        let biv = x.outer(z)*(num(rright)-num(rleft))*0.01;

        world.objects[0].accelerate(biv);

        world.update();

        let mut target = display.draw();
        use glium::Surface;
        target.clear_color_and_depth((0.0,0.0,0.0,1.0),1.0);
        let x = view_rot.rotate(Vector::<f32>::e1()).normalize();
        let y = view_rot.rotate(Vector::<f32>::e2()).normalize();
        let z = view_rot.rotate(Vector::<f32>::e3()).normalize();
        let pos = (-view_pos).vector();
        let (x,y,z) = (x.vector(), y.vector(), z.vector());
        use std::fmt::Debug;
        fn prl<T: Debug>(a: T) -> T {
            println!("{:?}",a);
            a
        }
        world.draw(&mut DrawContext{
            display: &display,
            target: &mut target,
            perspective: cgmath::conv::array4x4(perspective),
            view: [[x[0],x[1],x[2],0.0],
                   [y[0],y[1],y[2],0.0],
                   [z[0],z[1],z[2],0.0],
                   [pos[0],pos[1], pos[2],1.0]],    
        });
        target.finish().unwrap();

        events_loop.poll_events(|event| {
            if let glium::glutin::Event::WindowEvent {event, ..} = event {
                use glium::glutin::WindowEvent::*;
                match event {
                    Closed => closed = true,
                    Resized(x,y) => perspective = cgmath::perspective(cgmath::Rad(std::f32::consts::PI/2.0), (x as f32)/(y as f32), 1.0, 65536.0)
,
                    KeyboardInput{input: glium::glutin::KeyboardInput{
                        virtual_keycode: Some(code), state: key_state, ..},
                    ..} => {
                        use glium::glutin::ElementState::*;
                        let pressed = match key_state {
                            Pressed => true,
                            Released => false
                        };
                        use glium::glutin::VirtualKeyCode::*;
                        match code {
                            Escape => closed = pressed,
                            Left => left = pressed,
                            Right => right = pressed,
                            Up => up = pressed,
                            Down => down = pressed,
                            A => rleft = pressed,
                            D => rright = pressed,
                            W => rup = pressed,
                            S => rdown = pressed,
                            _ => ()
                        }
                    },
                    _ => ()
                }
            }
        });
        {
            use std::thread;
            use std::time::Duration;
            
            let fps = 30;

            let wait = Duration::from_millis(1000/fps);

            let elapsed = now.elapsed();

            if wait>elapsed {
                thread::sleep(wait-elapsed);
            } else {
                println!{"Low performance!"};
            }
        }
    }
}

fn main() {
    

    let mesh = Mesh::from_obj("sphere.obj");
    let objects = vec![
        Spatial::<f32,Ball<f32>> {
            pos: Vector::new(0.0, 0.0, -16.0),
            ..Default::default()
        },
        Spatial {
            pos: Vector::new(8.0, 0.0, -16.0),
            vel: Vector::new(-1.0, 0.0, 0.0)*0.125,
            shape: Ball {rad: 4.0},
            frag: 4.0,
            ..Default::default()
        },
        Spatial {
            pos: Vector::new(-8.0, 0.0, -16.0),
            vel: Vector::new(1.0, 0.0, 0.0)*0.125,
            frag: 0.125,
            ..Default::default()
        },
        Spatial {
            pos: Vector::new(0.0, 8.0, -16.0),
            vel: Vector::new(0.0, -1.0, 0.0)*0.125,
            ..Default::default()
        },
        Spatial {
            pos: Vector::new(0.0, -4.0, -16.0),
            vel: Vector::new(0.0, 1.0, 0.0)*0.125,
            ..Default::default()
        },
        Spatial {
            pos: Vector::new(0.0, -72.0, -16.0),
            vel: Vector::new(0.0, 0.0, 0.0)*0.125,
            rot: Bivector::new(0.0,2.0.powi(-8),0.0),
            frag: 2.0.powi(-8),
            shape: Ball {rad: 64.0},
            ..Default::default()
        },
    ];
    let world = World {objects};
    
    game_loop(world)
    
}


